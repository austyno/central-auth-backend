const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
require('dotenv/config');

// == Extended from: https://swagger.io/specification/#infoObject
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'SP Registration API',
            description: "Shield Prime Registration API",
            contact: {
                name: "",
                email: "",
            },
            servers: ["http://localhost:7000"]
        }
    },
    // ['.routes/*.js']
    // apis: ["server.js"]
    apis: ["./routes/*.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));


//========= Middlewares: Make Node use Body Parser to Parse the body of requests 
app.use(bodyParser.json());
app.use(cors());


//==================== Import Middleware and Use It =============================
// Import MiddleWare Routes 
const authRoute = require('./routes/auth');
// Use the middleware routes
app.use('/auth', authRoute);


//=============== Main App Route ==============

// app.get('/home', (req, res) => {
//     console.log("request")
//     res.status(200).send('Home Route');
// });

//=========== Connect to DB ===================
mongoose.connect(
                process.env.DB_CONNECTION, 
                { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true }
                )
                .then(() => console.log('MongoDB connected!'))
                .catch(err => console.log('Mongo Error: ' + err));

// Listening
app.listen(7000, () => {
    console.log('Server is now Running on 7000')
});