const Joi = require('@hapi/joi');

// SignUp Validation 
const SignUpValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        phone: Joi.number().min(9).required(),
        password: Joi.string().min(6).required()
    });

    return schema.validate(data);
};

// Login Validation
const LoginValidation = (data) => {
    const schema = Joi.object({
        email: Joi.string().required().email(),
        password: Joi.string().min(6).required()
    });

    return schema.validate(data);
};



module.exports.SignUpValidation = SignUpValidation;
module.exports.LoginValidation = LoginValidation;