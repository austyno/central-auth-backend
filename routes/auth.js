const express = require('express');
const router = express.Router();
const User = require('../models/user.model');
const { SignUpValidation, LoginValidation } = require('../validation/validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');



/**
 * @swagger
 * /auth/signup:
 *  post:
 *      description: Create a shield Prime Account
 *      responses:
 *          '200':
 *              description: A successful response
 *          '400':
 *              description: Bad Request
 */
// SignUp Endpoint
router.post(`/signup`, async (req, res) => {
    // Validation
    const { error } = SignUpValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // Check if email already exists in DB
    const emailExists = await User.findOne({email: req.body.email});
    if(emailExists) return res.status(400).json('Email already exists!');

    // Hash Password
    const salt = bcrypt.genSaltSync(10);
    const hashed_password = bcrypt.hashSync(req.body.password, salt);


    const user = new User({
                            name: req.body.name,
                            email: req.body.email,
                            phone: req.body.password,
                            password: hashed_password
                            });
    // Save User
    try {
        const userData = await user.save();
        res.status(200).json({user: userData._id});
    } catch (error) {
        res.status(400).json(error);
    }
});




// SignIn Endpoint
/**
 * @swagger
 * /auth/login:
 *  post:
 *      description: Login to a shield Prime Account
 *      responses:
 *          '200':
 *              description: A successful response
 *          '400':
 *              description: Bad Request
 *      parameter: [
 *          "email":"email",
 *          "password":"pasword"
 *          ]
 */
router.post(`/login`, async (req, res) => {
    // Validation
    const { error } = LoginValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    // Check if the Email exists in DB
    const user = await User.findOne({email: req.body.email});
    if(!user) return res.status(400).json('Email or Password is Wrong');

    // Check if Password is Correct
    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if(!validPassword) return res.status(400).json('Invalid Password')

    // Create, Assign a token to the user (i.e used for login in the frontend)
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET);
    res.header('token', token).json({id: user._id, Token: token});
    // LOG USER IN
    res.json('Logged In');
});




module.exports = router;